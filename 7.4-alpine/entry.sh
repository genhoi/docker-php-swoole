#!/bin/sh

set -e

# Check if user exists
if ! id -u $APP_USER > /dev/null 2>&1; then
  adduser -D -H -g "App user" -u $APP_USER_ID $APP_USER
fi

exec "$@"
